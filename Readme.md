# Change Data Capture in practice

Ejemplo de uso del patrón Change Data Capture (CDC) con Kafka Connect y conectores Debezium y Elasticsearch.  

El caso de uso está basado en el siguiente diagrama:  

![Diagrama caso de uso de ejemplo](./asset/diagram.png "Caso de uso de ejemplo")

La infraestructura necesaria está compuesta por:

- Fichero [docker-compose.yml](docker-compose.yml) que despliegue: Zookeeper, Kafka, Kafka Connect, Schema Registry, Mysql y Elasticsearch.
- La carpeta [plugins](./plugins): Ubicar en esta carpeta los conectores [debezium-connector-mysql](https://www.confluent.io/hub/debezium/debezium-connector-mysql) y [confluentinc-kafka-connect-elasticsearch-11.0.3](https://www.confluent.io/hub/confluentinc/kafka-connect-elasticsearch). Ambos pueden ser descargados desde [Confluent hub](https://www.confluent.io/hub/).
- La carpeta [data](./data): Contiene los script necesario para montar una base de datos de ejemplo. [Esquema de la BD](https://en.wikibooks.org/wiki/SQL_Exercises/The_Hospital).
- Contiene dos ficheros de configuración de los conectores de Kafka Connect. ([mysql-src-connector.json](./mysql-src-connector.json), [es-sink-connector.json](es-sink-connector.json)).

Para iniciar la infraestructura ejecutar: `docker-compose up -d`

Una iniciado correctamente se pueden configurar los conectores lanzando una petición http (**Importante:** los plugins deben haberse descargado y ubicados en la carpeta plugins.):

- `curl -X POST -H "Content-Type: application/json" --data "@mysql-src-connector.json" http://localhost:8083/connectors`

- `curl -X POST -H "Content-Type: application/json" --data "@es-sink-connector.json" http://localhost:8083/connectors`

Finalmente puedes verificar que todo funciona correctamente consultado el contenido del índice de Elasticsearch que se ha creado automáticamente con `curl http://localhost:9200/patient/_search?pretty`  
Se deben haber replicado todos los registros de la tabla Patient.

Una mayor descripción en el post: [https://jlgalvan-rojas.medium.com/change-data-capture-replicando-datos-en-tiempo-real-3dc873cfc1d8](https://jlgalvan-rojas.medium.com/change-data-capture-replicando-datos-en-tiempo-real-3dc873cfc1d8)