# Carpeta Plugins

Ubicar en esta carpeta los conectores [debezium-connector-mysql](https://www.confluent.io/hub/debezium/debezium-connector-mysql) y [confluentinc-kafka-connect-elasticsearch-11.0.3](https://www.confluent.io/hub/confluentinc/kafka-connect-elasticsearch). Ambos pueden ser descargados desde [Confluent hub](https://www.confluent.io/hub/).
